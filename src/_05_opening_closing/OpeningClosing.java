package _05_opening_closing;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class OpeningClosing {

	private File file;
	private BufferedImage image;
	private BufferedImage grayScaleImage;
	private int[][] pixels;
	private int[][] rDecimal;
	private int[][] gDecimal;
	private int[][] bDecimal;
	private int[][] gray;
	private int[][] gray2;
	private int[][] gray3;
	private int[][] gray4;
	private int[][] gray5;
	
	public final int threshold = 128;
	public final int[][] structElements = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
	public final int[][] structElements1 = { { 1, 1, 1 }, { 1, 0, 1 }, { 1, 1, 1 } };
	public final int[][] structElements2 = { { 1, 1, 1 }, { 0, 0, 1 }, { 0, 0, 1 } };
	public final int[][] structElements3 = { { 0, 1, 1 }, { 0, 0, 1 }, { 0, 1, 1 } };
	public final int[][] structElements4 = { { 0, 0, 1 }, { 0, 0, 1 }, { 1, 1, 1 } };
	public final int[][] structElements5 = { { 0, 0, 0 }, { 1, 0, 1 }, { 1, 1, 1 } };
	public final int[][] structElements6 = { { 1, 0, 0 }, { 1, 0, 0 }, { 1, 1, 1 } };
	public final int[][] structElements7 = { { 1, 1, 0 }, { 1, 0, 0 }, { 1, 1, 0 } };
	public final int[][] structElements8 = { { 1, 1, 1 }, { 1, 0, 0 }, { 1, 0, 0 } };
	public final int[][] struct = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }};
	// };

	public OpeningClosing(File file) {
		this.file = file;
		initialize();
	}

	public void initialize() {
		setImageAndGrayScaleImage();
		pixels = diziOlustur();
		rDecimal = diziOlustur();
		gDecimal = diziOlustur();
		bDecimal = diziOlustur();
		gray = diziOlustur();
		gray2 = diziOlustur();
		gray3 = diziOlustur();
		gray4 = diziOlustur();
		gray5 = diziOlustur();
		
		pixelsAndRGB();
		setGraysInMonochrome(gray);
		copyArray(gray2, gray);
	}
	
	public int[][] diziOlustur() {
		return new int[image.getWidth()][image.getHeight()];
	}
	
	public void matrisinTersi(int[][] grays) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if (gray[i][j] == 1) {
					grays[i][j] = 0;
				} else {
					grays[i][j] = 1;
				}
			}
		}
//		copyArray(gray4, grays);
	}
	
	public void thinning(){
		thin(structElements);
	}

	private void thin(int[][] structElements) {
		hitOrMissTransform(structElements);
		matrisinTersi(gray5);
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if (gray[i][j] == gray5[i][j]) {
					gray[i][j] = 0;
				} // gray[i][j] == 0 && 
				else {
					gray[i][j] = 1;
				}
			}
		}
		copyArray(gray4, gray);
	}

	public void hitOrMissTransform(int[][] structElements) {
		matrisinTersi(gray3); // tersi gray3'te
		copyArray(gray2, gray);
		erosion(gray, gray2, structElements);
		copyArray(gray4, gray3);
		erosion(gray3, gray4, struct);
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if(gray2[i][j] == gray4[i][j]) {
					gray5[i][j] = 1;
				}
			}
		}
		copyArray(gray4, gray5);
	}
	
	public void boundary() {
		erosion(gray, gray2, structElements);
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if (gray[i][j] == 0 && gray[i][j] == gray2[i][j]) {
					gray[i][j] = 1;
				}
				// else if(gray[i][j] != 0 && gray[i][j] != gray2[i][j]){
				// gray3[i][j] = 0;
				// }
			}
		}
	}

	public void opening() {
		erosion(gray, gray2, structElements);
		copyArray(gray3, gray2);
		dilation(gray2, gray3);
		copyArray(gray4, gray3);
	}

	public void closing() {
		dilation(gray, gray2);
		copyArray(gray3, gray2);
		erosion(gray2, gray3, structElements);
		copyArray(gray4, gray3);
	}

	private void copyArray(int[][] gray3, int[][] gray2) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				gray3[i][j] = gray2[i][j];
			}
		}
	}

	public void outputMonochrome(File outputFile) {

//		monochromeImage(gray);
//		monochromeImage(gray2);
//		monochromeImage(gray3);
		monochromeImage(gray4);
//		monochromeImage(gray5);
		
		Color gColor = null;
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				gColor = new Color(gray4[i][j], gray4[i][j], gray4[i][j]);
				grayScaleImage.setRGB(i, j, gColor.getRGB());
			}
		}
		try {
			ImageIO.write(grayScaleImage, "jpg", outputFile);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Bitti :)");
		// initialize();
	}

	public void monochromeImage(int[][] gray) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if (gray[i][j] == 1) {
					gray[i][j] = 255;
				} else if (gray[i][j] == 0) {
					gray[i][j] = 0;
				}
			}
		}
	}

	public void setImageAndGrayScaleImage() {
		try {
			image = ImageIO.read(file);
			grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void setGraysInMonochrome(int[][] gray) {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j]) / 3;
				if (gray[i][j] > threshold) {
					gray[i][j] = 1;
				} else if (gray[i][j] <= threshold) {
					gray[i][j] = 0;
				}
				// gray2[i][j] = gray[i][j];
			}
		}
	}

	public void pixelsAndRGB() {
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
	}

	public void dilation(int[][] gray, int[][] gray2) {
		for (int i = 0; i + 2 < gray.length; i++) {
			for (int j = 0; j + 2 < gray[i].length; j++) {
				if (structElements[0][0] == gray[i][j] || structElements[0][1] == gray[i][j + 1]
						|| structElements[0][2] == gray[i][j + 2] || structElements[1][0] == gray[i + 1][j]
						|| structElements[1][1] == gray[i + 1][j + 1] || structElements[1][2] == gray[i + 1][j + 2]
						|| structElements[2][0] == gray[i + 2][j] || structElements[2][1] == gray[i + 2][j + 1]
						|| structElements[2][2] == gray[i + 2][j + 2]) {
					gray2[i + 1][j + 1] = 0;
				} else {
					gray2[i + 1][j + 1] = 1;
				}
			}
		}
	}

	public void erosion(int[][] gray, int[][] gray2, int[][] structsElements) {
		for (int i = 0; i + 2 < gray.length; i++) {
			for (int j = 0; j + 2 < gray[i].length; j++) {
				if (structsElements[0][0] == gray[i][j] && structsElements[0][1] == gray[i][j + 1]
						&& structsElements[0][2] == gray[i][j + 2] && structsElements[1][0] == gray[i + 1][j]
						&& structsElements[1][1] == gray[i + 1][j + 1] && structsElements[1][2] == gray[i + 1][j + 2]
						&& structsElements[2][0] == gray[i + 2][j] && structsElements[2][1] == gray[i + 2][j + 1]
						&& structsElements[2][2] == gray[i + 2][j + 2]) {
					gray2[i + 1][j + 1] = 0;
				} else {
					gray2[i + 1][j + 1] = 1;
				}
			}
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public BufferedImage getImage() {
		return image;
	}

	public BufferedImage getGrayScaleImage() {
		return grayScaleImage;
	}

	public int[][] getPixels() {
		return pixels;
	}

	public int[][] getrDecimal() {
		return rDecimal;
	}

	public int[][] getgDecimal() {
		return gDecimal;
	}

	public int[][] getbDecimal() {
		return bDecimal;
	}

	public int[][] getGray() {
		return gray;
	}

	public int[][] getGray2() {
		return gray2;
	}

	public int[][] getGray3() {
		return gray3;
	}
	public int[][] getGray4() {
		return gray4;
	}
	public int[][] getStructElements() {
		return structElements;
	}

}
