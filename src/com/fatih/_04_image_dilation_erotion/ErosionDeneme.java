package com.fatih._04_image_dilation_erotion;

public class ErosionDeneme {

	public static void main(String[] args) {
		
		int[][] binaryImage = {{1,0,0,0,0,1,0,0}, {0,1,1,1,0,0,0,0}, {1,1,1,1,0,0,0,1}, {0,1,1,1,0,0,0,0}, {0,0,0,1,1,1,0,0},
				{1,1,0,0,0,0,1,1}, {1,1,1,0,1,1,0,0}, {0,0,1,1,0,1,0,1}};
		
		int[][] dilationResult = {{1,0,0,0,0,1,0,0}, {0,0,0,0,0,0,0,0}, {1,1,1,1,0,0,0,1}, {0,1,0,0,0,0,0,0}, {0,0,0,1,1,1,0,0},
				{1,1,0,0,0,0,1,1}, {1,1,1,0,1,1,0,0}, {0,0,1,1,0,1,0,1}};
		
		int[] struct = {1,1,1};
		
		int[][] struct2 = {{1,1,1},{1,1,1},{1,1,1}};
		
		display(dilationResult);
		
		erosion(binaryImage, dilationResult, struct2);
		
		System.out.println("\n");
		
		display(dilationResult);
		
	}
	private static void erosion(int[][] binaryImage, int[][] dilationResult, int[][] struct2) {
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (struct2[0][0] == binaryImage[i][j] & struct2[0][1] == binaryImage[i][j+1] & struct2[0][2] == binaryImage[i][j+2] & 
						struct2[1][0] == binaryImage[i+1][j] & struct2[1][1] == binaryImage[i+1][j+1] & struct2[1][2] == binaryImage[i+1][j+2] &
						struct2[2][0] == binaryImage[i+2][j] & struct2[2][1] == binaryImage[i+2][j+1] & struct2[2][2] == binaryImage[i+2][j+2]) 
				{
					dilationResult[i+1][j+1] = 1;
				}else {
					dilationResult[i+1][j+1] = 0;
				}
			}
		}
	}
	
	private static void display(int[][] resim) {
		for (int i = 0; i < resim.length; i++) {
			for (int j = 0; j < resim[i].length; j++) {
				System.out.print(resim[i][j] + " ");
			}
			System.out.println();
		}
	}
}
