package com.fatih._04_image_dilation_erotion;

public class DilationDeneme {

	public static void main(String[] args) {
		
		int[][] binaryImage = {{1,0,0,0,0,1,0,0}, {0,0,0,0,0,0,0,0}, {1,1,1,1,0,0,0,1}, {0,1,0,0,0,0,0,0}, {0,0,0,1,1,1,0,0},
				{1,1,0,0,0,0,1,1}, {1,1,1,0,1,1,0,0}, {0,0,1,1,0,1,0,1}};
		
		int[][] dilationResult = {{1,0,0,0,0,1,0,0}, {0,0,0,0,0,0,0,0}, {1,1,1,1,0,0,0,1}, {0,1,0,0,0,0,0,0}, {0,0,0,1,1,1,0,0},
				{1,1,0,0,0,0,1,1}, {1,1,1,0,1,1,0,0}, {0,0,1,1,0,1,0,1}};
		
		int[] struct = {1,1,1};
		
		int[][] struct2 = {{1,1,1},{1,1,1},{1,1,1}};
		
		display(dilationResult);
		
		dilation(binaryImage, dilationResult, struct2);
		
//		for (int i = 0; i < resim.length; i++) {
//			for (int j = 0; j+2 < resim[i].length; j++) {
//				if (struct[0] == resim[i][j] | struct[1] == resim[i][j+1] | struct[2] == resim[i][j+2]) {
//					resim2[i][j+1] = 1;
//				}else {
//					resim2[i][j+1] = 0;
//				}
//			}
//		}
		
//		Graphics2D graphic = grayScaleImage.createGraphics();
//      graphic.drawImage(image, 0, 0, Color.GRAY, null);
//      graphic.dispose();
      
//		for (int i = 0; i < width; i++) {
//			for (int j = 0; j < height; j++) {
//				int p = grayScaleImage.getRGB(i, j);
//				int r = (p>>16) & 0xFF;
//				int g = (p>>8) & 0xFF;
//				int b = p & 0xFF;
//				
//				int gr = (r+g+b)/3;
//				System.out.print(gr + " ");
//			}
//			System.out.println();
//		}
		
		System.out.println("\n");
		
		display(dilationResult);
		
	}

	private static void dilation(int[][] binaryImage, int[][] dilationResult, int[][] struct2) {
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (struct2[0][0] == binaryImage[i][j] | struct2[0][1] == binaryImage[i][j+1] | struct2[0][2] == binaryImage[i][j+2] | 
						struct2[1][0] == binaryImage[i+1][j] | struct2[1][1] == binaryImage[i+1][j+1] | struct2[1][2] == binaryImage[i+1][j+2] |
						struct2[2][0] == binaryImage[i+2][j] | struct2[2][1] == binaryImage[i+2][j+1] | struct2[2][2] == binaryImage[i+2][j+2]) 
				{
					dilationResult[i+1][j+1] = 1;
				}else {
					dilationResult[i+1][j+1] = 0;
				}
			}
		}
	}
	
	private static void display(int[][] resim) {
		for (int i = 0; i < resim.length; i++) {
			for (int j = 0; j < resim[i].length; j++) {
				System.out.print(resim[i][j] + " ");
			}
			System.out.println();
		}
	}
	
}
