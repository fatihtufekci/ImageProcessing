package com.fatih._04_image_dilation_erotion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Deneme1 {

	public static void main(String[] args) {
		
		File file = new File("/home/fatih/Desktop/fatih400.jpg");
		BufferedImage image = null;
		BufferedImage grayScaleImage = null;
		
		
		try {
			image = ImageIO.read(file);
			grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(),
					BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		int height = image.getHeight();
		int width = image.getWidth();

		int[][] pixels = new int[width][height];

		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];
		
		int[][] gray = new int[width][height];

		int[][] gray2 = new int[width][height];
		
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j])/3;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray2[i][j] = 0;
			}
		}
		
		localBinaryPatterns(gray, gray2);
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color gColor = new Color(gray2[i][j], gray2[i][j], gray2[i][j]);
				grayScaleImage.setRGB(i, j, gColor.getRGB());
			}
		}

		File output = new File("/home/fatih/Desktop/dilation3.jpg");
		try {
			
            ImageIO.write(grayScaleImage, "jpg", output);
            
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private static void localBinaryPatterns(int[][] binaryImage, int[][] dilationResult) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (binaryImage[i][j] >= binaryImage[i+1][j+1]) {
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i][j+1] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+1][j] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				
				if(binaryImage[i+1][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j+1] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				System.out.println();
				int a = Integer.parseInt(s.toString());
				int b;
				b = binaryToDecimal(a);
				
				dilationResult[i+1][j+1] = b;
				
				s.delete(0, s.length());
				
			}
		}
	}
	
	private static int binaryToDecimal(int sayi) {

		String s = String.valueOf(sayi);
		int d = 0;
		int toplam = 0;
		int count = s.length() - 1;
		char f;
		String z = "";

		for (int i = 0; i < s.length(); i++) {
			f = s.charAt(i);
			z = String.valueOf(f);
			if (z.equals("1")) {
				d = (int) Math.pow(2, count);
				toplam = toplam + d;
			}
			count--;
		}
		return toplam;
	}

}
