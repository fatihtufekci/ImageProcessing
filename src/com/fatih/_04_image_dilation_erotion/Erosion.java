package com.fatih._04_image_dilation_erotion;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Erosion {

	public static void main(String[] args) {
		File file = new File("/home/fatih/Desktop/gs2.jpg");
		BufferedImage image = null;

		try {
			image = ImageIO.read(file);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		int height = image.getHeight();
		int width = image.getWidth();

		int[][] pixels = new int[width][height];

		int[][] structElements = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };

		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];

		int[][] r = new int[width][height];
		int[][] g = new int[width][height];
		int[][] b = new int[width][height];
		
		int[][] r1 = new int[width][height];
		int[][] g1 = new int[width][height];
		int[][] b1 = new int[width][height];

		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r[i][j] = decimalToBinary(rDecimal[i][j]);
				g[i][j] = decimalToBinary(gDecimal[i][j]);
				b[i][j] = decimalToBinary(bDecimal[i][j]);
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r1[i][j] = r[i][j];
				g1[i][j] = g[i][j];
				b1[i][j] = b[i][j];
			}
		}
		
		
		erosion(r, r1, structElements);
		display(r1);
		System.out.println("Fatih");
//		for (int i = 0; i < width; i++) {
//			for (int j = 0; j < height; j++) {
//				rDecimal[i][j] = binaryToDecimal(r1[i][j]);
//				gDecimal[i][j] = binaryToDecimal(g1[i][j]);
//				bDecimal[i][j] = binaryToDecimal(b1[i][j]);
//			}
//		}
//
//		for (int i = 0; i < width; i++) {
//			for (int j = 0; j < height; j++) {
//				pixels[i][j] = (rDecimal[i][j] << 16) | (gDecimal[i][j] << 8) | (bDecimal[i][j]);
//				image.setRGB(i, j, pixels[i][j]);
//			}
//		}
//
//		File output = new File("/home/fatih/Desktop/erosion.jpg");
//		try {
//			ImageIO.write(image, "jpg", output);
//		} catch (IOException e) {
//			System.out.println(e.getMessage());
//		}
	}
	private static void display(int[][] resim) {
		for (int i = 0; i < resim.length; i++) {
			for (int j = 0; j < resim[i].length; j++) {
				System.out.print(resim[i][j] + " ");
			}
			System.out.println();
		}
	}
	private static void erosion(int[][] binaryImage, int[][] dilationResult, int[][] struct2) {
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (struct2[0][0] == binaryImage[i][j] & struct2[0][1] == binaryImage[i][j+1] & struct2[0][2] == binaryImage[i][j+2] & 
						struct2[1][0] == binaryImage[i+1][j] & struct2[1][1] == binaryImage[i+1][j+1] & struct2[1][2] == binaryImage[i+1][j+2] &
						struct2[2][0] == binaryImage[i+2][j] & struct2[2][1] == binaryImage[i+2][j+1] & struct2[2][2] == binaryImage[i+2][j+2]) 
				{
					dilationResult[i+1][j+1] = 1;
				}else {
					dilationResult[i+1][j+1] = 0;
				}
			}
		}
	}

	private static int binaryToDecimal(int sayi) {

		String s = String.valueOf(sayi);
		int d = 0;
		int toplam = 0;
		int count = s.length() - 1;
		char f;
		String z = "";

		for (int i = 0; i < s.length(); i++) {
			f = s.charAt(i);
			z = String.valueOf(f);
			if (z.equals("1")) {
				d = (int) Math.pow(2, count);
				toplam = toplam + d;
			}
			count--;
		}
		return toplam;
	}

	private static int decimalToBinary(int sayi) {
		int[] kalan;
		int x = 0, counter = 0;

		int z = sayi;

		while (z > 0) {
			z /= 2;
			counter++;
		}

		kalan = new int[counter];

		while (sayi > 0) {
			kalan[x] = sayi % 2;
			sayi /= 2;
			x++;
		}
		String binarySayi = "";

		for (int q = kalan.length - 1; q >= 0; q--) {
			binarySayi += String.valueOf(kalan[q]);
		}
		// char b = hjk.charAt(hjk.length() - 1);
		// String l = hjk.substring(2);
		// System.out.println(b + " " + l);

		int a = 255;

		if (binarySayi.equals("")) {
		} else {
			a = Integer.parseInt(binarySayi);
		}

		return a;
	}

}
