package com.fatih._04_image_dilation_erotion;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Dilation2 {

	public static void main(String[] args) {

		File file = new File("/home/fatih/Desktop/fatih400.jpg");
		BufferedImage image = null;
		BufferedImage grayScaleImage = null;
		
		
		try {
			image = ImageIO.read(file);
			grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(),
					BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		int height = image.getHeight();
		int width = image.getWidth();

		int[][] pixels = new int[width][height];

		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];
		
		int[][] gray = new int[width][height];

		int[][] gray2 = new int[width][height];
		
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j])/3;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if(gray[i][j] > 127) {
					gray[i][j] = 1;
				}else if(gray[i][j] <=127) {
					gray[i][j] = 0;
				}
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray2[i][j] = gray[i][j];
			}
		}
		
		int[][] structElements = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
		
		int[][] structElements2 = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
		
		
		dilation(gray, gray2, structElements);
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if(gray[i][j]==1) {
					gray[i][j] = 255;
				}else if(gray[i][j]==0) {
					gray[i][j] = 0;
				}
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if(gray2[i][j]==1) {
					gray2[i][j] = 255;
				}else if(gray2[i][j]==0) {
					gray2[i][j] = 0;
				}
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color gColor = new Color(gray2[i][j], gray2[i][j], gray2[i][j]);
				grayScaleImage.setRGB(i, j, gColor.getRGB());
			}
		}

		File output = new File("/home/fatih/Desktop/dilation3.jpg");
		try {
			
            ImageIO.write(grayScaleImage, "jpg", output);
            
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private static void denemee(int[][] binaryImage, int[][] dilationResult) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (binaryImage[i][j] >= binaryImage[i+1][j+1]) {
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i][j+1] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+1][j] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				
				if(binaryImage[i+1][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j+1] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				if(binaryImage[i+2][j+2] >= binaryImage[i+1][j+1]){
					s.append(1);
				}else {
					s.append(0);
				}
				
				int a = Integer.parseInt(s.toString());
				int b;
				b = binaryToDecimal(a);
				if(b>127) {
					dilationResult[i+1][j+1] = 1;
				}else {
					dilationResult[i+1][j+1] = 0;
				}
			}
		}
	}
	
	private static void dilation(int[][] binaryImage, int[][] dilationResult, int[][] struct2) {
		for (int i = 0; i+2 < binaryImage.length; i++) {
			for (int j = 0; j+2 < binaryImage[i].length; j++) {
				if (struct2[0][0] == binaryImage[i][j] || struct2[0][1] == binaryImage[i][j+1] || struct2[0][2] == binaryImage[i][j+2] || 
						struct2[1][0] == binaryImage[i+1][j] || struct2[1][1] == binaryImage[i+1][j+1] || struct2[1][2] == binaryImage[i+1][j+2] ||
						struct2[2][0] == binaryImage[i+2][j] || struct2[2][1] == binaryImage[i+2][j+1] || struct2[2][2] == binaryImage[i+2][j+2]) 
				{
					dilationResult[i+1][j+1] = 0;
				}else {
					dilationResult[i+1][j+1] = 1;
				}
			}
		}
	}
	
	private static void display(int[][] resim) {
		for (int i = 0; i < resim.length; i++) {
			for (int j = 0; j < resim[i].length; j++) {
				System.out.print(resim[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static int binaryToDecimal(int sayi) {

		String s = String.valueOf(sayi);
		int d = 0;
		int toplam = 0;
		int count = s.length() - 1;
		char f;
		String z = "";

		for (int i = 0; i < s.length(); i++) {
			f = s.charAt(i);
			z = String.valueOf(f);
			if (z.equals("1")) {
				d = (int) Math.pow(2, count);
				toplam = toplam + d;
			}
			count--;
		}
		return toplam;
	}

	private static int decimalToBinary(int sayi) {
		int[] kalan;
		int x = 0, counter = 0;

		int z = sayi;

		while (z > 0) {
			z /= 2;
			counter++;
		}

		kalan = new int[counter];

		while (sayi > 0) {
			kalan[x] = sayi % 2;
			sayi /= 2;
			x++;
		}
		String binarySayi = "";

		for (int q = kalan.length - 1; q >= 0; q--) {
			binarySayi += String.valueOf(kalan[q]);
		}
		// char b = hjk.charAt(hjk.length() - 1);
		// String l = hjk.substring(2);
		// System.out.println(b + " " + l);

		int a = 255;

		if (binarySayi.equals("")) {
		} else {
			a = Integer.parseInt(binarySayi);
		}

		return a;
	}

}
