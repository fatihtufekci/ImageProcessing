package com.fatih._01_image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Deneme1 {
	public static void main(String[] args) {
		BufferedImage im = new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_BINARY);
		im.setRGB(10, 10, Color.WHITE.getRGB());

		try {
			ImageIO.write(im, "png", new File("/home/fatih/Desktop/image.png"));
		} catch (IOException e) {
			System.out.println("Some exception occured " + e);
		}
	}
}
