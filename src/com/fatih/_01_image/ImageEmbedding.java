package com.fatih._01_image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageEmbedding {

	public static void main(String[] args) {
		File original = new File("/home/fatih/Desktop/fatih400.jpg");
		ByteArrayOutputStream baos = null;
		BufferedImage image = null;
		try {
			image = ImageIO.read(original);
			// baos = new ByteArrayOutputStream();
			// ImageIO.write(image, "jpg", baos);
			// byte[] jpgByteArray = baos.toByteArray();
			// StringBuilder sb = new StringBuilder();
			// for (byte by : jpgByteArray)
			// sb.append(Integer.toBinaryString(by & 0xFF));
			//
			// System.out.println(sb.toString());
			// System.out.println(jpgByteArray.length);

			// BufferedImage grayScaleImage = new BufferedImage(image.getWidth(),
			// image.getHeight(),
			// BufferedImage.TYPE_BYTE_BINARY);
			int[][] piksel = new int[image.getHeight()][image.getWidth()];
			for (int i = 0; i < image.getHeight(); i++) {
				for (int j = 0; j < image.getWidth(); j++) {
					int p = image.getRGB(j, i);
					System.out.print(p + " ");
					piksel[i][j] = p;
				}
				System.out.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
