package com.fatih._01_image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageGrayScales {

	public static void main(String[] args) {

		File original = new File("/home/fatih/Desktop/sari.jpg");
		BufferedImage image = null;
		
		try {
			image = ImageIO.read(original);
			BufferedImage grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(),
					BufferedImage.TYPE_INT_RGB);

			for (int i = 0; i < image.getWidth(); i++) {
				for (int j = 0; j < image.getHeight(); j++) {
					Color c = new Color(image.getRGB(i, j));
					int r = c.getRed();
					int g = c.getGreen();
					int b = c.getBlue();
					int a = c.getAlpha();

					int gr = (r + g + b) / 3;
					
					System.out.print(gr + "  ");
					Color gColor = new Color(gr, gr, gr);
					grayScaleImage.setRGB(i, j, gColor.getRGB());

				}
				System.out.println();
			}
			ImageIO.write(grayScaleImage, "png", new File("/home/fatih/Desktop/gray4.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
