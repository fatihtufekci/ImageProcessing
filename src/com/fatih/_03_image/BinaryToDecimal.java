package com.fatih._03_image;

public class BinaryToDecimal {

	public static void main(String[] args) {

		String s = "10110";
		int d = 0;
		int toplam = 0;
		int count = s.length() - 1;
		char f;
		String z = "";

		for (int i = 0; i < s.length(); i++) {
			f = s.charAt(i);
			z = String.valueOf(f);
			if (z.equals("1")) {
				d = (int) Math.pow(2, count);
				toplam = toplam + d;
			}
			count--;
		}

		System.out.println(toplam);
	}

}
