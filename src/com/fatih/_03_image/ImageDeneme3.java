package com.fatih._03_image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageDeneme3 {
	File original = new File("/home/fatih/Desktop/gs.jpg");
	BufferedImage image = null;

	int width = 0;
	int height = 0;
	int[][] pixels = null;
	int[][] r;
	int[][] g;
	int[][] b;

	public ImageDeneme3() {
		try {
			image = ImageIO.read(original);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		this.width = image.getWidth();
		this.height = image.getHeight();
		this.pixels = new int[this.width][this.height];
		rgbValue();
	}
	
	private void rgbValue() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = image.getRGB(i, j);
				r[i][j] = (pixels[i][j] >> 16) & 0xff;
				g[i][j] = (pixels[i][j] >> 8) & 0xff;
				b[i][j] = pixels[i][j] & 0xff;
			}
		}
	}
	
	private void rgbLSB() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				
			}
		}
	}
	
	public static void display(int[][] dizi) {
		for(int i=0; i<dizi.length-1; i++) {
			for(int j=0; j<dizi[i].length-1; j++) {
				System.out.print(dizi[i][j] + " ");
			}
			System.out.println();
		}
	}

	public BufferedImage getImage() {
		return image;
	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}

	public int[][] getPixels() {
		return pixels;
	}
	
	public File getOriginal() {
		return original;
	}

	public int[][] getR() {
		return r;
	}

	public int[][] getG() {
		return g;
	}

	public int[][] getB() {
		return b;
	}

}
