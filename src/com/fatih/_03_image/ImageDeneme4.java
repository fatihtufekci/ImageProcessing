package com.fatih._03_image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageDeneme4 {

	public static void main(String[] args) {

		long start = System.currentTimeMillis();

		File original = null;
		BufferedImage image = null;

		File hideFile = null;
		BufferedImage hideImage = null;
		BufferedImage grayScaleImage = null;

		try {
			original = new File("/home/fatih/Desktop/gs2.jpg");
			image = ImageIO.read(original);
			grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(),
					BufferedImage.TYPE_INT_RGB);

			hideFile = new File("/home/fatih/Desktop/bayrak.png");
			hideImage = ImageIO.read(hideFile);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		int width = 400;
		int height = 400;

		int[][] pixels = new int[width][height];

		int[][] hidePixels = new int[width][height];

		int p = 0;

		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];

		int[][] hideRDecimal = new int[width][height];
		int[][] hideGDecimal = new int[width][height];
		int[][] hideBDecimal = new int[width][height];

		int[][] r = new int[width][height];
		int[][] g = new int[width][height];
		int[][] b = new int[width][height];

		int[][] hideR = new int[width][height];
		int[][] hideG = new int[width][height];
		int[][] hideB = new int[width][height];

		int[][] hideRMSB = new int[width][height];
		int[][] hideGMSB = new int[width][height];
		int[][] hideBMSB = new int[width][height];

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < hideImage.getWidth(); i++) {
			for (int j = 0; j < hideImage.getHeight(); j++) {
				hidePixels[i][j] = hideImage.getRGB(i, j);
				hideRDecimal[i][j] = (hidePixels[i][j] >> 16) & 0xff;
				hideGDecimal[i][j] = (hidePixels[i][j] >> 8) & 0xff;
				hideBDecimal[i][j] = hidePixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < hideRDecimal.length; i++) {
			for (int j = 0; j < hideRDecimal[i].length; j++) {
				System.out.print(hideRDecimal[i][j] + "  ");
			}
			System.out.println();
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r[i][j] = 0;
			}
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r[i][j] = decimalToBinary(rDecimal[i][j]);
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r[i][j] = decimalToBinary(rDecimal[i][j]);
				g[i][j] = decimalToBinary(gDecimal[i][j]);
				b[i][j] = decimalToBinary(bDecimal[i][j]);

				hideR[i][j] = decimalToBinary(hideRDecimal[i][j]);
				hideG[i][j] = decimalToBinary(hideGDecimal[i][j]);
				hideB[i][j] = decimalToBinary(hideBDecimal[i][j]);

				hideRMSB[i][j] = msb(hideR[i][j]);
				hideGMSB[i][j] = msb(hideG[i][j]);
				hideBMSB[i][j] = msb(hideB[i][j]);
			}
		}
		int[][] gray = new int[width][height];
		int[][] hideGray = new int[width][height];
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j])/3;
			}
		}
		for (int i = 0; i < hideImage.getWidth(); i++) {
			for (int j = 0; j < hideImage.getHeight(); j++) {
				hideGray[i][j] = (hideRDecimal[i][j] + hideGDecimal[i][j] + hideBDecimal[i][j])/3;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if(gray[i][j] > 127) {
					gray[i][j] = 1;
				}else if(gray[i][j] <=127) {
					gray[i][j] = 0;
				}
			}
		}

//		for (int i = 0; i < hideImage.getWidth(); i++) {
//			for (int j = 0; j < hideImage.getHeight(); j++) {
//				if(hideGray[i][j] > 60) {
//					hideGray[i][j] = 1;
//				}else if(hideGray[i][j] <=60) {
//					hideGray[i][j] = 0;
//				}
//			}
//		}
		
		
		
		for (int i = 0; i < rDecimal.length; i++) {
			for (int j = 0; j < rDecimal[i].length; j++) {
				if(rDecimal[i][j] > 110) {
					rDecimal[i][j] = 1;
				}else if(rDecimal[i][j] <=110) {
					rDecimal[i][j] = 0;
				}
			}
		}
		
//		for (int i = 0; i < rDecimal.length; i++) {
//			for (int j = 0; j < rDecimal[i].length; j++) {
//				System.out.print(rDecimal[i][j] + "  ");
//			}
//			System.out.println();
//		}

		int[][] rc = new int[width][height];
//		rc = resimGom(rDecimal, hideGray);
		rc = resimGom(r, hideRMSB);

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				rDecimal[i][j] = binaryToDecimal(rc[i][j]);
				gDecimal[i][j] = binaryToDecimal(g[i][j]);
				bDecimal[i][j] = binaryToDecimal(b[i][j]);
			}
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = (rDecimal[i][j] << 16) | (gDecimal[i][j] << 8) | (bDecimal[i][j]);
				image.setRGB(i, j, pixels[i][j]);
			}
		}

		File output = new File("/home/fatih/Desktop/output.jpg");
		try {
			ImageIO.write(image, "jpg", output);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		long stop = System.currentTimeMillis();
		System.out.println(stop - start);
		
		int[][] rLSB = new int[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				rLSB[i][j] = lsb(rc[i][j]);
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				rc[i][j] = binaryToDecimal(rc[i][j]);
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color gColor = new Color(rc[i][j], gDecimal[i][j], bDecimal[i][j]);
				grayScaleImage.setRGB(i, j, gColor.getRGB());
			}
		}

		File outputF = new File("/home/fatih/Desktop/gomulu.jpg");
		try {

			ImageIO.write(grayScaleImage, "jpg", outputF);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	private static int[][] resimGom(int[][] pixel, int[][] lsbDizi) {
		String h = "";
		StringBuilder sb = new StringBuilder(h);
		String ekle;

		for (int i = 0; i < pixel.length - 1; i++) {
			for (int j = 0; j < pixel[i].length - 1; j++) {
				sb.delete(0, sb.length());
				h = String.valueOf(lsbDizi[i][j]);
				sb.append(pixel[i][j]);
				sb.replace(sb.length() - 1, sb.length(), h);
				ekle = sb.toString();
				pixel[i][j] = Integer.parseInt(ekle);
			}
		}

		return pixel;
	}
	
	private static int[][] resimGom2(int[][] pixel, int[][] lsbDizi) {
		
		int c = 0;
		
		for (int i = 0; i < pixel.length - 1; i++) {
			for (int j = 0; j < pixel[i].length - 1; j++) {
				c = (pixel[i][j]>>7);
//				pixel[i][j]>>7 = lsbDizi[i][j];
			}
		}

		return pixel;
	}

//	private static void display(int[][] dizi) {
//		for (int i = 0; i < dizi.length - 1; i++) {
//			for (int j = 0; j < dizi[i].length - 1; j++) {
//				System.out.print(dizi[i][j] + " ");
//			}
//			System.out.println();
//		}
//	}

	// private static int lsb(int sayi) {
	// String h = String.valueOf(sayi);
	// char z = h.charAt(h.length() - 1);
	// String c = String.valueOf(z);
	// int a = Integer.parseInt(c);
	//
	// return a;
	// }

	private static int msb(int sayi) {
		String h = String.valueOf(sayi);
		char z = h.charAt(0);
		String c = String.valueOf(z);
		int a = Integer.parseInt(c);

		return a;
	}
	
	private static int lsb(int sayi) {
		String h = String.valueOf(sayi);
		char z = h.charAt(h.length()-1);
		String c = String.valueOf(z);
		int a = Integer.parseInt(c);

		return a;
	}

	private static int binaryToDecimal(int sayi) {

		String s = String.valueOf(sayi);
		int d = 0;
		int toplam = 0;
		int count = s.length() - 1;
		char f;
		String z = "";

		for (int i = 0; i < s.length(); i++) {
			f = s.charAt(i);
			z = String.valueOf(f);
			if (z.equals("1")) {
				d = (int) Math.pow(2, count);
				toplam = toplam + d;
			}
			count--;
		}
		return toplam;
	}

	private static int decimalToBinary(int sayi) {
		int[] kalan;
		int x = 0, counter = 0;

		int z = sayi;

		while (z > 0) {
			z /= 2;
			counter++;
		}

		kalan = new int[counter];

		while (sayi > 0) {
			kalan[x] = sayi % 2;
			sayi /= 2;
			x++;
		}
		String binarySayi = "";

		for (int q = kalan.length - 1; q >= 0; q--) {
			binarySayi += String.valueOf(kalan[q]);
		}
		// char b = hjk.charAt(hjk.length() - 1);
		// String l = hjk.substring(2);
		// System.out.println(b + " " + l);

		int a = 255;

		if (binarySayi.equals("")) {
		} else {
			a = Integer.parseInt(binarySayi);
		}

		return a;
	}

}
