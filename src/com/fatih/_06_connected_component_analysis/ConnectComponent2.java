package com.fatih._06_connected_component_analysis;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ConnectComponent2 {

	public static void main(String[] args) {
		
		File file = new File("/home/fatih/Desktop/sari.jpg");
		BufferedImage image = null;
		BufferedImage grayScaleImage = null;
		int[][] gray;
		int[][] gray2;
		
		try {
			image = ImageIO.read(file);
			grayScaleImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		int height = image.getHeight();
		int width = image.getWidth();
		int[][] pixels = new int[width][height];
		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];
		
		gray = new int[width][height];
		gray2 = new int[width][height];
		
		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j]) / 3;
			}
		}
		
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (gray[i][j] > 127) {
					gray[i][j] = 1;
				} else if (gray[i][j] <= 127) {
					gray[i][j] = 0;
				}
			}
		}

		connComponent2(gray);
		
		int max = 0;
		for (int i = 0; i < gray.length; i++) {
			for (int j = 0; j < gray[i].length; j++) {
				if(j!=0) {
					max = gray[i][j-1];
					if(gray[i][j] > max) {
						max = gray[i][j];
					}
				}
			}
		}
		
		
		System.out.println("Sınıf sayısı: " + max);
		
	}

	private static void display(int[][] gray) {
		for (int i = 0; i < gray.length; i++) {
			for (int j = 0; j < gray[i].length; j++) {
				System.out.print(gray[i][j] + "  ");
			}
			System.out.println();
		}
	}

	private static void connComponent2(int[][] gray) {
		int x=1;
		for (int i = 0; i < gray.length; i++) {
			for (int j = 0; j < gray[i].length; j++) {
				if(gray[i][j] == 1) {
					if(j==0 || gray[i][j-1] == x) {
						gray[i][j] = x;
					}else {
						x++;
						gray[i][j] = x;
					}
				}
			}
		}
		
		for (int i = 0; i< gray.length; i++) {
			for (int j = 0; j< gray[i].length; j++) {
				if(gray[i][j] != 0) {
					if(j!=0 && i!=0) {
						if(gray[i-1][j] != 0) {
							gray[i][j] = gray[i-1][j]; 
						}
					}
				}
			}
		}
		
	}
	
	
	private static void connComponent(int[][] gray, int[][] gray2) {
		int count = 1;
		for (int i = 0; i < gray.length; i++) {
			for (int j = 0; j+1 < gray[i].length; j++) {
				if(gray[i][j] == 1) {
					if(gray[i][j+1] == 1) {
						gray2[i][j] = count;
					}else {
						count++;
						gray2[i][j] = count;
					}
				}
			}
		}
		int min=0;
//		for (int i = 0; i + 2 < gray2.length; i++) {
//			for (int j = 0; j + 2 < gray2[i].length; j++) {
//				if(gray[i+1][j+1] > gray[i][j] || gray[i+1][j+1] > gray[i][j+1] || gray[i+1][j+1] > gray[i][j+2]) {
//					min = gray[i][j];
//					if(gray[i][j+1] < min) 
//						min = gray[i][j+1];
//					if(gray[i][j+2] < min)
//						min = gray[i][j+2];
//					
//					gray[i+1][j+1] = min;
//				}
//			}
//		}
		int x=1;
		for (int i = 0; i < gray2.length; i++) {
			for (int j = 0; j < gray2[i].length; j++) {
				if(gray2[i][j] == 1) {
					if(j==0 || gray2[i][j-1] == x) {
						gray2[i][j] = x;
					}else {
						x++;
						gray2[i][j] = x;
					}
				}
			}
		}
		
		
		for (int i = 0; i< gray2.length; i++) {
			for (int j = 0; j< gray2[i].length; j++) {
				if(gray2[i][j] != 0) {
					if(j!=0 && i!=0) {
						if(gray2[i-1][j] != 0) {
							gray2[i][j] = gray2[i-1][j]; 
						}
					}
				}
			}
		}
	}

}
