package com.fatih._06_connected_component_analysis;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ConnectComponent {
	
	private static int[] count;
	
	public static void main(String[] args) {
		File file = new File("/home/fatih/Desktop/fatih400.jpg");
		BufferedImage image = null;
		BufferedImage grayScaleImage = null;
		int[][] gray;
		int[][] gray2;
		int[][] gray3;
		int[][] structElements = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
		try {
			image = ImageIO.read(file);
			grayScaleImage = new BufferedImage(image.getWidth(),image.getHeight(),
					BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		count = new int[image.getWidth()];
		for (int i = 0; i < count.length; i++) {
			count[i] = 0;
		}
		
		int height = image.getHeight();
		int width = image.getWidth();
		int[][] pixels = new int[width][height];
		int[][] rDecimal = new int[width][height];
		int[][] gDecimal = new int[width][height];
		int[][] bDecimal = new int[width][height];

		gray = new int[width][height];
		gray2 = new int[width][height];

		for (int i = 0; i < pixels.length; i++) {
			for (int j = 0; j < pixels[i].length; j++) {
				pixels[i][j] = image.getRGB(i, j);

				rDecimal[i][j] = (pixels[i][j] >> 16) & 0xff;
				gDecimal[i][j] = (pixels[i][j] >> 8) & 0xff;
				bDecimal[i][j] = pixels[i][j] & 0xff;
			}
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray[i][j] = (rDecimal[i][j] + gDecimal[i][j] + bDecimal[i][j]) / 3;
			}
		}

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (gray[i][j] > 127) {
					gray[i][j] = 1;
				} else if (gray[i][j] <= 127) {
					gray[i][j] = 0;
				}
			}
		}
		
		gray3 = new int[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gray2[i][j] = gray[i][j];
				gray3[i][j] = 0;
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				System.out.print(gray[i][j] + " ");
			}
			System.out.println();
		}
		
		int c = connectComponent(gray2, gray3, structElements);
		
		
		resmiBas(grayScaleImage, gray3);

	}

	public static void resmiBas(BufferedImage grayScaleImage, int[][] gray2) {
		for (int i = 0; i < gray2.length; i++) {
			for (int j = 0; j < gray2[i].length; j++) {
				if(gray2[i][j]==1) {
					gray2[i][j] = 255;
				}else if(gray2[i][j]==0) {
					gray2[i][j] = 0;
				}
			}
		}
		
		for (int i = 0; i < gray2.length; i++) {
			for (int j = 0; j < gray2[i].length; j++) {
				Color gColor = new Color(gray2[i][j], gray2[i][j], gray2[i][j]);
				grayScaleImage.setRGB(i, j, gColor.getRGB());
			}
		}

		File output = new File("/home/fatih/Desktop/resim.jpg");
		try {
			
            ImageIO.write(grayScaleImage, "jpg", output);
            
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static int connectComponent(int[][] binaryImage, int[][] connectResult, int[][] struct2) {
		int c = 0;
		for (int i = 0; i + 2 < binaryImage.length; i++) {
			for (int j = 0; j + 2 < binaryImage[i].length; j++) {
				if (binaryImage[i + 1][j + 1] == 1) {
					if (struct2[0][0] == binaryImage[i][j] || struct2[0][1] == binaryImage[i][j + 1]
							|| struct2[0][2] == binaryImage[i][j + 2] || struct2[1][0] == binaryImage[i + 1][j]
							|| struct2[1][2] == binaryImage[i + 1][j + 2] || struct2[2][0] == binaryImage[i + 2][j]
							|| struct2[2][1] == binaryImage[i + 2][j + 1]
							|| struct2[2][2] == binaryImage[i + 2][j + 2]) {
						
						connectResult[i + 1][j + 1] = count[c];
//						System.out.println(connectResult[i + 1][j + 1]);
						if (struct2[0][0] == binaryImage[i][j]) {
							connectResult[i][j] = count[c];
//							System.out.println(connectResult[i][j]);
						}
						
						if (struct2[0][1] == binaryImage[i][j + 1]) {
							connectResult[i][j + 1] = count[c];
//							System.out.println(connectResult[i][j + 1]);
						}
						
						if (struct2[0][2] == binaryImage[i][j + 2]) {
							connectResult[i][j + 2] = count[c];
//							System.out.println(connectResult[i][j + 2]);
						}
						
						if (struct2[1][0] == binaryImage[i + 1][j]) {
							connectResult[i + 1][j] = count[c];
//							System.out.println(connectResult[i + 1][j]);
						}
						
						if (struct2[1][2] == binaryImage[i + 1][j + 2]) {
							connectResult[i + 1][j + 2] = count[c];
//							System.out.println(connectResult[i + 1][j + 2]);
						}
						
						if (struct2[2][0] == binaryImage[i + 2][j]) {
							connectResult[i + 2][j] = count[c];
//							System.out.println(connectResult[i + 2][j]);
						}
						
						if (struct2[2][1] == binaryImage[i + 2][j + 1]) {
							connectResult[i + 2][j + 1] = count[c];
//							System.out.println(connectResult[i + 2][j + 1]);
						}
						
						if (struct2[2][2] == binaryImage[i + 2][j + 2]) {
							connectResult[i + 2][j + 2] = count[c];
//							System.out.println(connectResult[i + 2][j + 2]);
						}
						
					} else {
//						System.out.println("dsadas");
						count[c] = ++c;
					}
				}
			}
		}
		return c;
	}

}
