package com.fatih._02_image;

import java.awt.image.BufferedImage;

public class Main {
	
	public static void main(String[] args) {
		BufferedImage image = PhotoPixels.readFromFile("/home/fatih/Desktop/gs.jpg");
        double[][] pixel = PhotoPixels.imageToDouble(image);
        PhotoPixels.yazdir(pixel);
        PhotoScene rs = new PhotoScene(image);
	}
	
}
