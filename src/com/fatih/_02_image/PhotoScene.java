package com.fatih._02_image;

import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class PhotoScene extends JFrame{
	
	PhotoPanel pn1;
	
	public PhotoScene(String path) {
		pn1 = new PhotoPanel(path);
		Int();
	}
	
	public PhotoScene(BufferedImage image) {
		pn1 = new PhotoPanel(image);
		Int();
	}
	
	private void Int() {
		add(pn1);
        setSize(pn1.getImage().getWidth(), pn1.getImage().getHeight());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
	}
	
}
