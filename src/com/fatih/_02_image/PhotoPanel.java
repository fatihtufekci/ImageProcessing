package com.fatih._02_image;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class PhotoPanel extends JFrame{
	private BufferedImage image;

    public PhotoPanel(String path) {
        try {
        	image = ImageIO.read(new File(path));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public PhotoPanel(BufferedImage img) {
        this.image = img;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
    
    public void paintComponent(Graphics g){
        g.drawImage(this.image, 0, 0, null);
        repaint();
    }
}
